"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const engexp_1 = require("../src/engexp");
describe("custom", () => {
    it("Match Fav car companies", () => {
        const e = new engexp_1.EngExp()
            .startOfLine()
            .match("Ferrari").or("Lamborghini").or("Aston martin")
            .endOfLine()
            .asRegExp();
        chai_1.expect(e.test("Ferrari")).to.be.true;
        chai_1.expect(e.test("Lamborghini")).to.be.true;
        chai_1.expect(e.test("Honda")).to.be.false;
    });
    it("throw an error with Mismatched nesting levels", () => {
        chai_1.expect(() => {
            new engexp_1.EngExp().beginCapture().then(new engexp_1.EngExp().endCapture());
        }).to.throw("Mismatched nesting levels");
        chai_1.expect(() => {
            new engexp_1.EngExp().endCapture();
        }).to.throw("Mismatched nesting levels");
    });
    it("throws an error when levels and capture groups are interleaved", () => {
        chai_1.expect(() => {
            new engexp_1.EngExp().beginLevel().endCapture();
        }).to.throw("Mismatched nesting levels");
        chai_1.expect(() => {
            new engexp_1.EngExp().beginCapture().endLevel();
        }).to.throw("Mismatched nesting levels");
    });
    it("works with another EngExp in .then()", () => {
        // put the body of the test here
        const e = new engexp_1.EngExp()
            .match("Burger ")
            .maybe("with fries ")
            .then("is ")
            .then(new engexp_1.EngExp()
            .match("not at all ")
            .optional())
            .then("healthy").asRegExp();
        const result = e.exec("Burger with fries is not at all healthy, Burger with fries is healthy, Salad is healthy");
        chai_1.expect(e.test("Burger with fries is not at all healthy")).to.be.true;
        chai_1.expect(e.test("Burger with fries is healthy")).to.be.true;
        chai_1.expect(e.test("Salad is healthy")).to.be.false;
        chai_1.expect(result[0]).to.be.equal("Burger with fries is not at all healthy");
    });
    it("Everybody Jump", () => {
        const e = new engexp_1.EngExp()
            .startOfLine()
            .then("Everybody ")
            .beginLevel()
            .then("jump ")
            .then("and scream ")
            .repeated(2, 4)
            .endLevel()
            .endOfLine()
            .asRegExp();
        chai_1.expect(e.test("Everybody jump and scream jump and scream ")).to.be.true;
        chai_1.expect(e.test("Everybody jump and scream jump and scream jump and scream jump and scream ")).to.be.true;
        chai_1.expect(e.test("Everybody jump and scream ")).to.be.false;
    });
});
//# sourceMappingURL=custom.js.map