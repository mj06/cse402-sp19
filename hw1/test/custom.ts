import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {

    it("Match Fav car companies", () => {

        const e = new EngExp()
            .startOfLine()
            .match("Ferrari").or("Lamborghini").or("Aston martin")
            .endOfLine()
            .asRegExp()

        expect(e.test("Ferrari")).to.be.true;
        expect(e.test("Lamborghini")).to.be.true;
        expect(e.test("Honda")).to.be.false;
    });

    it("throw an error with Mismatched nesting levels", () => {
        
        expect(() => {
            new EngExp().beginCapture().then(
                new EngExp().endCapture()
            )
        }).to.throw("Mismatched nesting levels");
        
        expect(() => {
            new EngExp().endCapture()
        }).to.throw("Mismatched nesting levels");
    });

    it("throws an error when levels and capture groups are interleaved", () => {
        
        expect(() => {
            new EngExp().beginLevel().endCapture();
        }).to.throw("Mismatched nesting levels");

        expect(() => {
            new EngExp().beginCapture().endLevel();
        }).to.throw("Mismatched nesting levels");

    });

    it("works with another EngExp in .then()", () => {
        
        // put the body of the test here
        const e = new EngExp()
        .match("Burger ")
        .maybe("with fries ")
        .then("is ")
        .then(
            new EngExp()
                .match("not at all ")
                .optional()
        )
        .then("healthy").asRegExp();
            
        const result = e.exec("Burger with fries is not at all healthy, Burger with fries is healthy, Salad is healthy");
        
        expect(e.test("Burger with fries is not at all healthy")).to.be.true;
        expect(e.test("Burger with fries is healthy")).to.be.true;
        expect(e.test("Salad is healthy")).to.be.false;
        expect(result[0]).to.be.equal("Burger with fries is not at all healthy");
    });

    it("Everybody Jump", () => {
        
        const e = new EngExp()
            .startOfLine()
            .then("Everybody ")
            .beginLevel()
            .then("jump ")
            .then("and scream ")
            .repeated(2, 4)
            .endLevel()
            .endOfLine()
            .asRegExp();

            expect(e.test("Everybody jump and scream jump and scream ")).to.be.true;
            expect(e.test("Everybody jump and scream jump and scream jump and scream jump and scream ")).to.be.true;
            expect(e.test("Everybody jump and scream ")).to.be.false;
    });
});
