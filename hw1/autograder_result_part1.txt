
> hw1@2019.1.1 pretest_grader /mnt/c/Users/maazs/OneDrive - UW/Teaching/cse401-sp19/grading/hw1/mj06/hw1
> tsc -p tsc_grader.json


> hw1@2019.1.1 test_grader /mnt/c/Users/maazs/OneDrive - UW/Teaching/cse401-sp19/grading/hw1/mj06/hw1
> mocha test/grader.js



  autograder:
pattern (?:\d)+
    1) should throw if unbalanced endLevel
pattern (?:\d)+
    ✓ should throw if unbalanced beginLevel
    ✓ should throw if unbalanced endCapture
pattern (?:\d)+
    ✓ should throw if unbalanced beginCapture
pattern (?:\d)+
    ✓ should throw for beginLevel / endCapture
pattern (?:\d)+
    ✓ should throw for beginCapture / endLevel
    ✓ balanced but mismatched parenthesis
pattern (?:x)(?:a)|(?:b)
pattern ^(?:(?:x)(?:a)|(?:b))$
    ✓ (minimal) correct precedence of or
pattern (?:a)|(?:b)(?:c)
pattern ^(?:(?:a)|(?:b)(?:c))$
    2) (minimal) correct precedence of or, part 2
pattern ((?:a)|(?:b))
pattern ^(?:((?:a)|(?:b)))$
    ✓ (minimal) or with groups
pattern |(?:a)
pattern ^(?:|(?:a))$
    ✓ empty or matches empty string
pattern ((?:a)(|(?:b))(?:c))
pattern ^(?:((?:a)(|(?:b))(?:c)))$
    ✓ precedence of or with groups and empty or
pattern \d|(?:b)
pattern ^(?:\d|(?:a)(?:\ )(?:\d|(?:b))|(?:z))$
    3) should separate alternatives


  10 passing (30ms)
  3 failing

  1) autograder:
       should throw if unbalanced endLevel:

      AssertionError: expected [Function: e] to throw 'EngExpError' but 'TypeError: Cannot read property \'0\' of undefined' was thrown
      + expected - actual

      -TypeError: Cannot read property '0' of undefined
      +EngExpError
      
      at Context.it (test/grader.js:19:34)

  2) autograder:
       (minimal) correct precedence of or, part 2:

      match(a).or(b).then(c) should match ac
      + expected - actual

      -false
      +true
      
      at Context.it (test/grader.js:134:84)

  3) autograder:
       should separate alternatives:

      digit().or('a').then(' ').then(new EngExp().digit().or('b')).or('z') should match '1 b'
      + expected - actual

      -false
      +true
      
      at Context.it (test/grader.js:197:72)




npm ERR! Linux 4.4.0-17134-Microsoft
npm ERR! argv "/usr/bin/node" "/usr/bin/npm" "run" "test_grader"
npm ERR! node v8.10.0
npm ERR! npm  v3.5.2
npm ERR! code ELIFECYCLE
npm ERR! hw1@2019.1.1 test_grader: `mocha test/grader.js`
npm ERR! Exit status 3
npm ERR! 
npm ERR! Failed at the hw1@2019.1.1 test_grader script 'mocha test/grader.js'.
npm ERR! Make sure you have the latest version of node.js and npm installed.
npm ERR! If you do, this is most likely a problem with the hw1 package,
npm ERR! not with npm itself.
npm ERR! Tell the author that this fails on your system:
npm ERR!     mocha test/grader.js
npm ERR! You can get information on how to open an issue for this project with:
npm ERR!     npm bugs hw1
npm ERR! Or if that isn't available, you can get their info via:
npm ERR!     npm owner ls hw1
npm ERR! There is likely additional logging output above.

npm ERR! Please include the following file with any support request:
npm ERR!     /mnt/c/Users/maazs/OneDrive - UW/Teaching/cse401-sp19/grading/hw1/mj06/hw1/npm-debug.log
