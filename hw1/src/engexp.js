"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EngExpError extends Error {
}
exports.EngExpError = EngExpError;
class EngExp {
    constructor() {
        this.flags = "m";
        this.pattern = "";
        this.groupIndexes = [[0, false]];
    }
    // you can add new fields here
    // don't change sanitize()
    static sanitize(s) {
        if (s instanceof EngExp)
            return s;
        else {
            return s.replace(/[^A-Za-z0-9_]/g, "\\$&");
        }
    }
    // don't change these either
    toString() {
        return this.asRegExp().source;
    }
    valueOf() {
        return this.asRegExp().source;
    }
    withFlags(flags) {
        if (/[^gimuy]/g.test(flags))
            throw new EngExpError("invalid flags");
        this.flags = "".concat(...new Set(flags));
        return this;
    }
    // asRegExp() will always be called before using your EngExp as a RegExp,
    // or before converting it into a string (for example to include via ``
    // template literals in another EngExp). You might want to take advantage
    // of this to check for errors...
    asRegExp() {
        let lastElement = this.groupIndexes[this.groupIndexes.length - 1];
        if (lastElement[0] != 0 || this.groupIndexes.length != 1) {
            throw new EngExpError("Malformed Expression");
        }
        console.log("pattern", this.pattern);
        return new RegExp(this.pattern, this.flags);
    }
    // There is a bug somewhere in this code... can you find it?
    match(literal) {
        return this.then(literal);
    }
    then(pattern) {
        this.pattern += `(?:${EngExp.sanitize(pattern)})`;
        return this;
    }
    startOfLine() {
        this.pattern += "^";
        return this;
    }
    endOfLine() {
        this.pattern += "$";
        return this;
    }
    zeroOrMore(pattern) {
        if (pattern)
            return this.then(pattern.zeroOrMore());
        else {
            this.pattern = this.pattern.substring(0, this.groupIndexes[this.groupIndexes.length - 1][0]) + `(?:${this.pattern.substring(this.groupIndexes[this.groupIndexes.length - 1][0])})*`;
            return this;
        }
    }
    oneOrMore(pattern) {
        if (pattern)
            return this.then(pattern.oneOrMore());
        else {
            this.pattern = this.pattern.substring(0, this.groupIndexes[this.groupIndexes.length - 1][0]) + `(?:${this.pattern.substring(this.groupIndexes[this.groupIndexes.length - 1][0])})+`;
            return this;
        }
    }
    optional() {
        this.pattern = this.pattern.substring(0, this.groupIndexes[this.groupIndexes.length - 1][0]) + `(?:${this.pattern.substring(this.groupIndexes[this.groupIndexes.length - 1][0])})?`;
        return this;
    }
    maybe(pattern) {
        this.pattern += `(?:${EngExp.sanitize(pattern)})?`;
        return this;
    }
    anythingBut(characters) {
        this.pattern += `[^${EngExp.sanitize(characters)}]*`;
        return this;
    }
    digit() {
        this.pattern += "\\d";
        return this;
    }
    repeated(from, to) {
        let pat = "";
        if (to === undefined) {
            pat = `(?:${this.pattern.substring(this.groupIndexes[this.groupIndexes.length - 1][0])}){${from}}`;
        }
        else {
            pat = `(?:${this.pattern.substring(this.groupIndexes[this.groupIndexes.length - 1][0])}){${from},${to}}`;
        }
        console.log("pat", pat);
        this.pattern = this.pattern.substring(0, this.groupIndexes[this.groupIndexes.length - 1][0]) + pat;
        return this;
    }
    multiple(pattern, from, to) {
        if (to === undefined) {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${from}}`;
        }
        else {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${from},${to}}`;
        }
        return this;
    }
    // you need to implement these five operators:
    or(pattern) {
        this.pattern += `|(?:${EngExp.sanitize(pattern)})`;
        return this;
    }
    beginLevel() {
        this.groupIndexes.push([this.pattern.length, false]);
        this.pattern += `(?:`;
        return this;
    }
    endLevel() {
        if (this.groupIndexes.pop()[1] && this.groupIndexes.length > 0) {
            throw new EngExpError("Mismatched nesting levels");
        }
        this.pattern += `)`;
        return this;
    }
    beginCapture() {
        this.groupIndexes.push([this.pattern.length, true]);
        this.pattern += `(`;
        return this;
    }
    endCapture() {
        if (!this.groupIndexes.pop()[1]) {
            throw new EngExpError("Mismatched nesting levels");
        }
        this.pattern += `)`;
        return this;
    }
}
exports.EngExp = EngExp;
// Essentailly, a NamedRegExp is like a RegExp, but when you call its exec() method
// you get back an array with an extra field, groups, which is a hashmap from the
// names of capture groups to what those groups matched. You can access it like this:
//
//     let r = new EngExp().beginCapture("justf").then("f").endCapture().then("oo").asRegExp()
//     r.exec("foo").groups["justf"]  ==  "f"
//
// See the tests at the end of test/engexp.ts for more exampls of usage.
//# sourceMappingURL=engexp.js.map